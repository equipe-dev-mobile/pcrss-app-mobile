import { ActivityIndicator, FlatList, StyleSheet, Text, View, Button } from 'react-native';
import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

export default function App() {

  const Stack = createNativeStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{ title: 'Accueil'}}
        />
        <Stack.Screen name="Details" component={DetailsScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'whitesmoke'
  },
  footer:{
    height: '50px',
    width: '100%',
    border: '2px solid gray',
    marginBottom: '0px',
    textAlign: 'center',
  },
  details:{
    marginBottom: 20,

  }
});

const HomeScreen = ({ navigation }) => {

  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(true);

  const getMovies = async () => {
     try {
      const response = await fetch('https://api.spacexdata.com/v3/capsules');
      const json = await response.json();
      setData(json);
    } catch (error) {
      console.error("erreur");
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getMovies();
  }, []);

  return (
    <View style={styles.container}>
    <Text>Liste des capsules </Text>
      {isLoading ? <ActivityIndicator/> : (
        <FlatList
          data={data}
          keyExtractor={({ capsule_serial }, index) => capsule_serial }
          renderItem={({ item }) => (
            <View style={{ flex: 1, flexDirection: 'column', margin: 1 }}>
              <div style={{ height: 'auto', width: '90%'}}>
                <div className="card" style={{ width: '20rem'}}>
                  <div className="card-body" >
                    <View style={{ flex: 1, flexDirection: 'column', margin: 1, textAlign: 'center'}}>
                      <h2 className="card-title" style={{ color : 'green', fontStyle: 'bold'}}>Capsule {item.capsule_serial}</h2>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'column', margin: 1, textAlign: 'center' }}>
                    <div class="table-responsive">
                          <form class="container" style={{fontStyle: 'bold', fontFamily: 'serif'}}>
                            <div className="form-group row">
                              <label for="staticEmail" className="col-sm-2 col-form-label">ID : </label>
                              <label for="staticEmail" className="col-sm-2 col-form-label" style={{ color : 'green', fontStyle: 'bold'}}>{item.capsule_id} </label>
                            </div>

                            <div className="form-group row">
                              <label for="staticEmail" className="col-sm-2 col-form-label" >Serie : </label>
                              <label for="staticEmail" className="col-sm-2 col-form-label" style={{ color : 'green', fontStyle: 'bold'}}>{item.capsule_serial} </label>
                            </div>

                            <div className="form-group row">
                              <label for="staticEmail" className="col-sm-2 col-form-label">Status : </label>
                              <label for="staticEmail" className="col-sm-2 col-form-label" style={{ color : 'green', fontStyle: 'bold'}}>{item.status} </label>
                            </div>

                            <div className="form-group row">
                              <label for="staticEmail" className="col-sm-2 col-form-label">Type : </label>
                              <label for="staticEmail" className="col-sm-2 col-form-label" style={{ color : 'green', fontStyle: 'bold'}}>{item.type} </label>
                            </div>

                            
                          </form>
                      </div>
                    </View>
                    <View style={[{ width: "90%", margin: 10, backgroundColor: "red" }]}>
                      <Button
                        title="Voir plus"
                        onPress={() =>
                          navigation.navigate('Details', item)
                        }
                        color="#FF3D00"
                      />
                      </View>
                </div>
              </div>
              </div>
            </View>
        )}
          numColumns={1}
        />
      )}
      <View style={styles.footer}>
        <Text>Copyright @ 2022 - PCRSS app</Text>
      </View>
    </View>
    
  );
};

const DetailsScreen = ({ navigation, route }) => {
  const item = route.params;
  return (

    <View style={{ flex: 1, flexDirection: 'column', margin: 1, textAlign: 'center' }}>
                    <div class="table-responsive">
                    <h2 className="card-title" style={{ color : 'green', fontStyle: 'bold'}}>Capsule {item.capsule_serial}</h2>  
                    <form class="container" style={{ marginTop: '30px', fontStyle: 'bold', fontFamily: 'serif', fontSize: '1.2em'}}>
                            <div className="form-group row" style={ styles.details }>
                              <label for="staticEmail" className="col-sm-2 col-form-label">ID  </label><br/>
                              <label for="staticEmail" className="col-sm-2 col-form-label" style={{ color : 'green', fontStyle: 'bold'}}>{item.capsule_id} </label>
                            </div>

                            <div className="form-group row" style={ styles.details }>
                              <label for="staticEmail" className="col-sm-2 col-form-label">Serie  </label><br/>
                              <label for="staticEmail" className="col-sm-2 col-form-label" style={{ color : 'green', fontStyle: 'bold'}}>{item.capsule_serial} </label>
                            </div>

                            <div className="form-group row" style={ styles.details }>
                              <label for="staticEmail" className="col-sm-2 col-form-label">Details </label><br/>
                              <label for="staticEmail" className="col-sm-2 col-form-label" style={{ color : 'green', fontStyle: 'bold'}}>{item.details} </label>
                            </div>

                            <div className="form-group row" style={ styles.details }>
                              <label for="staticEmail" className="col-sm-2 col-form-label">Status  </label><br/>
                              <label for="staticEmail" className="col-sm-2 col-form-label" style={{ color : 'green', fontStyle: 'bold'}}>{item.status} </label>
                            </div>

                            <div className="form-group row" style={ styles.details }>
                              <label for="staticEmail" className="col-sm-2 col-form-label">Original Lunch  </label><br/>
                              <label for="staticEmail" className="col-sm-2 col-form-label" style={{ color : 'green', fontStyle: 'bold'}}>{item.original_launch} </label>
                            </div>

                            
                            <div className="form-group row" style={ styles.details }>
                              <label for="staticEmail" className="col-sm-2 col-form-label">Original Lunch Unix </label><br/>
                              <label for="staticEmail" className="col-sm-2 col-form-label" style={{ color : 'green', fontStyle: 'bold'}}>{item.original_launch_unix} </label>
                            </div>

                            <div className="form-group row" style={ styles.details }>
                              <label for="staticEmail" className="col-sm-2 col-form-label">Type </label><br/>
                              <label for="staticEmail" className="col-sm-2 col-form-label" style={{ color : 'green', fontStyle: 'bold'}}>{item.type} </label>
                            </div>
                          </form>
                      </div>
                    </View>
  );
};


